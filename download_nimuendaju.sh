wget -O src/geojson/nimuendaju.json 'http://portal.iphan.gov.br/geoserver/Nimuendaju/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Nimuendaju%3Atg_tribo_view&maxFeatures=50000&outputFormat=application%2Fjson'

wget -O src/geojson/quadrantes.json 'http://portal.iphan.gov.br/geoserver/Nimuendaju/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Nimuendaju%3Atb_quadrante&maxFeatures=50000&outputFormat=application%2Fjson'

wget -O src/geojson/tribos_quadrantes.json 'http://portal.iphan.gov.br/geoserver/Nimuendaju/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Nimuendaju%3Atb_tribo_quadrante&maxFeatures=50000&outputFormat=application%2Fjson'

wget -O src/geojson/referencias.json 'http://portal.iphan.gov.br/geoserver/Nimuendaju/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Nimuendaju%3Atb_referencia_bibliografica_nimuendaju&maxFeatures=50000&outputFormat=application%2Fjson'

wget -O src/geojson/tribos_referencias.json 'http://portal.iphan.gov.br/geoserver/Nimuendaju/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Nimuendaju%3Atb_tribo_referencia&maxFeatures=50000&outputFormat=application%2Fjson'

wget -O src/geojson/sitio_arqueologico.json 'http://portal.iphan.gov.br/geoserver/SICG/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=SICG%3Asitios_pol&maxFeatures=500000&outputFormat=application%2Fjson'

# wget -O src/geojson/sitio_arqueologico.json 'http://portal.iphan.gov.br/geoserver/SICG/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=SICG%3Asitios&maxFeatures=10000&outputFormat=application%2Fjson'
