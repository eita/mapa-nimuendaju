# Mapa Etno-Histórico Nimuendaju

## How to install:

1. Add file `src/config.js` with the following lines:
```
const SETTINGS = {
  MAPBOX_ACCESS_TOKEN: "{YOUR_MAPBOX_TOKEN_HERE}",
};

export default SETTINGS;
```

2. Run the bash script `download_numendaju.sh` to download data from server

3. Install all packages with `yarn install`

4. Now you can run the map with `yarn start`
