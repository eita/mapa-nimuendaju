from BaseETL import BaseETL

class EtniasUF(BaseETL):
    def setup(self):
        self._region_file = 'uf.json'
        self._url = 'http://portal.iphan.gov.br/geoserver/DBGEO/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=DBGEO%3Auf&maxFeatures=5000&outputFormat=application%2Fjson'
        self._column_name = 'id'
        self._table_name = 'uf'
        self._out_path = '../src/geojson/etnias_uf.json'
