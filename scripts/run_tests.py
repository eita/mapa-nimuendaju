import os, sys, unittest
sys.path.append('test/')
from TestOverlayETL import TestOverlayETL

test_classes = [TestOverlayETL]


testList = [unittest.TestLoader().loadTestsFromTestCase(test_class) for test_class in test_classes]

suite = unittest.TestSuite(testList)
runner = unittest.TextTestRunner(verbosity=3)
result = runner.run(suite)