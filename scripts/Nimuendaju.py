from BaseETL import BaseETL
import os, shutil

class Nimuendaju(BaseETL):
    def setup(self):
        self._region_file = 'nimuendaju.json'
        self._url = 'http://portal.iphan.gov.br/geoserver/Nimuendaju/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Nimuendaju%3Atg_tribo_view&maxFeatures=5000&outputFormat=application%2Fjson'

    def transform(self, update=True):
        if os.path.isfile(self._output_path) and not update:
            self.show_log(f'\nArquivo {self._output_path} já existia e será mantido. Para atualizar, utilize update=True')
        else:
            # gdf = gp.read_file(self._input_path)
            shutil.copyfile(self._input_path, self._output_path)

