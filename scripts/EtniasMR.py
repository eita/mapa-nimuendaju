from BaseETL import BaseETL

class EtniasMR(BaseETL):
    def setup(self):
        self._region_file = 'mr.json'
        # self._url = 'http://portal.iphan.gov.br/geoserver/DBGEO/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=DBGEO%3Auc&maxFeatures=5000&outputFormat=application%2Fjson'
        self._column_name = 'id'
        self._table_name = 'mr'
        self._out_path = '../src/geojson/etnias_mr.json'
