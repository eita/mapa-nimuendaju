from BaseETL import BaseETL
import geopandas as gp
import os
# import requests

class EtniasMunicipios(BaseETL):
    def setup(self):
        self._region_file = 'municipio.json'
        self._url = 'http://portal.iphan.gov.br/geoserver/DBGEO/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=DBGEO%3Amunicipios&maxFeatures=5000&outputFormat=application%2Fjson'
        self._column_name = 'id'
        self._table_name = 'municipio'
        self._out_path = '../src/geojson/etnias_municipios.json'
