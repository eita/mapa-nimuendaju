
import json, os, requests
import geopandas as gp
import geojson

from datetime import datetime

class BaseETL:
    def __init__(self, test=False, verbose=False):

        self._test = test
        self._verbose = verbose
        self.setup()

        if self._test:
            self._base_dir = 'test/ref_input'
        else:
            self._base_dir = 'geojson'
        
        self._input_dir = f'{self._base_dir}/input'
        self._output_dir = f'{self._base_dir}/output'
        self._input_path = f'{self._input_dir}/{self._region_file}'
        self._output_path = f'{self._output_dir}/{self._region_file}'

        self._nimuendaju_path = f'{self._input_dir}/nimuendaju.json'

        os.makedirs(self._input_dir,exist_ok=True)
        os.makedirs(self._output_dir,exist_ok=True)

    def setup(self):
        '''Deve ser definida nas classes derivadas'''

        msg = 'Deve ser definida nas classes derivadas'
        Warning(msg)

    def show_log(self, msg):
        if self._verbose:
            print(msg)
    
    def extract(self, update=False):

        if os.path.isfile(self._input_path) and not update:
            self.show_log(f'\nArquivo {self._input_path} já existia e será mantido. Para atualizar, utilize update=True')
        else:
            self.show_log(f'iniciando download do arquivo {self._region_file}...')
            r = requests.get(self._url)

            if r.status_code == 200:
                self.show_log('Download realizado com sucesso.')
            elif r.status_code == 404:
                raise ValueError(f'url não encontrada: {self._url}')

            with open(self._input_path, 'wb') as f:
                f.write(r.content)

    def transform(self, update=True):
        if os.path.isfile(self._output_path) and not update:
            self.show_log(f'\nArquivo {self._output_path} já existia e será mantido. Para atualizar, utilize update=True')
        else:
            gdf = gp.read_file(self._input_path)

            gdf = gdf.rename(columns = {'ID':'id','GEOCOD':'GEOCODIGO','NOME_AC':'nome','NOM_UC_C':'nome','BIO_NOM':'nome'})
            selected_cols = ['id','nome','geometry']
            # print(self._output_path, gdf.columns)
            
            # apenas para municipios
            if self._region_file == 'municipio.json':
                gdf = gdf.loc[gdf['POP_2010'].notna()]
                assert gdf.shape[0] == 5570
                mun_problem = [1712702, 4110706, 2109700, 5107958]
                for geocode in mun_problem:
                    gdf.loc[gdf['GEOCODIGO'] == geocode,'geometry'] = self.get_geometry(geocode)
                                        
            # if 'GEOCODIGO' in gdf.columns:
            #     gdf['GEOCODIGO'] = gdf['GEOCODIGO'].astype(int)
            #     selected_cols.append('GEOCODIGO')
            
            gdf = gdf[selected_cols].reset_index(drop=True).sort_values('id')


            gdf.to_file(self._output_path,driver="GeoJSON")
    
    def get_geometry(self, geocode:int):
    
        assert len(str(geocode)) == 7
        url = f'https://servicodados.ibge.gov.br/api/v3/malhas/municipios/{geocode}?formato=application/vnd.geo+json'
        r = requests.get(url)

        if r.status_code == 200:
            print(f'Geocódigo: {geocode}. Download realizado com sucesso.')
        elif r.status_code == 404:
            raise ValueError(f'url não encontrada: {url}')

        temp_file = 'geojson/temp.json'

        with open(temp_file, 'wb') as f:
            f.write(r.content)

        gdf_temp = gp.read_file(temp_file)
        os.remove(temp_file)
        return gdf_temp['geometry'].values

    def save_intersection_geojson(self):

        column_name = self._column_name
        new_column_name = f'{column_name}_{self._table_name}'.lower()

        id_column = f'id_tribo_{self._table_name}'
        tb_column = f'tb_tribo_{self._table_name}'

        gd_arcos = gp.read_file(self._nimuendaju_path)
        gd_region = gp.read_file(self._output_path)

        gd_arcos = gd_arcos[['id_tribo','geometry']].to_crs(4326)    #4674
        gd_region = gd_region[[column_name,'geometry']].to_crs(4326)
        gd_inter = gd_arcos.overlay(gd_region,keep_geom_type=False)

        table = gd_inter[['id_tribo',column_name]].reset_index()
        table = table.rename(columns={'index':id_column,column_name:new_column_name})

        features = table.apply( lambda row: geojson.Feature(geometry=None,
            id=f"{tb_column}.{row[id_column]}",
            properties = { id_column: int(row[id_column]),"id_tribo": int(row['id_tribo']),
            new_column_name: int(row[new_column_name])}),
            axis=1).tolist()
        
        feature_collection = geojson.FeatureCollection(features=features)
        n_features = len(table)
        timeStamp = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
        feature_collection['totalFeatures'] = n_features
        feature_collection['numberMatched'] = n_features
        feature_collection['numberReturned'] = n_features
        feature_collection['timeStamp'] = f'{timeStamp[:-3]}Z'
        feature_collection['crs'] = None

        with open(self._out_path, 'w', encoding='utf-8') as f:
            geojson.dump(feature_collection, f)
            print(f'Arquivo salvo em: {self._out_path}')

