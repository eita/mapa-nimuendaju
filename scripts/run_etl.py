from Nimuendaju import Nimuendaju
from EtniasBiomas import EtniasBiomas
from EtniasMunicipios import EtniasMunicipios
from EtniasMR import EtniasMR
from EtniasUC import EtniasUC
from EtniasUF import EtniasUF


db = Nimuendaju(verbose=True) # False True
update=True
# update=False
db.extract(update)
db.transform(update)

verbose=True # False True
db_list = []
db_list.append(EtniasBiomas(verbose=verbose))
db_list.append(EtniasMR(verbose=verbose))
db_list.append(EtniasMunicipios(verbose=verbose))
db_list.append(EtniasUC(verbose=verbose))
db_list.append(EtniasUF(verbose=verbose))

# update=True
update=False

for db in db_list:
    # db.extract(update)
    db.transform(update)
    db.save_intersection_geojson()
