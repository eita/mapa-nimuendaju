import nimuendaju from './nimuendaju.json';
import mr from './mr.json';
import tribos_mr from './tribos_mr.json';
import quadrantes from './quadrantes.json';
import tribos_quadrantes from './tribos_quadrantes.json';
import uf from './uf.json';
import tribos_uf from './tribos_uf.json';
import municipio from './municipio.json';
import tribos_municipio from './tribos_municipio.json';
import bioma from './bioma.json';
import tribos_bioma from './tribos_bioma.json';
import uc  from './uc.json';
import tribos_uc from './tribos_uc.json';
import referencias from './referencias.json';
import tribos_referencias from './tribos_referencias.json';
import ti  from './ti.json';
import sitio_arqueologico  from './sitio_arqueologico.json';

export {
    nimuendaju,
    mr,
    tribos_mr,
    bioma,
    tribos_bioma,
    uf,
    tribos_uf,
    municipio,
    tribos_municipio,
    uc,
    tribos_uc,
    quadrantes,
    tribos_quadrantes,
    referencias,
    tribos_referencias,
    ti,
    sitio_arqueologico,
};
