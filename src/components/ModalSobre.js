import {
    Row,
    Col,
    Modal,
    Tabs,
    Tab,
    Figure
} from 'react-bootstrap';

import camadas1_1 from '../imgs/camadas1_1.png';
import camadas1_2 from '../imgs/camadas1_2.png';
import camadas2_1 from '../imgs/camadas2_1.png';
import camadas3_1 from '../imgs/camadas3_1.png';
import camadas4_1 from '../imgs/camadas4_1.png';
import camadas5_1 from '../imgs/camadas5_1.png';

import componente1_1 from '../imgs/componente1_1.png';
import componente2_1 from '../imgs/componente2_1.png';
import componente2_2 from '../imgs/componente2_2.png';
import componente2_3 from '../imgs/componente2_3.png';
import componente3_1 from '../imgs/componente3_1.png';
import componente3_2 from '../imgs/componente3_2.png';
import componente4_1 from '../imgs/componente4_1.png';
import componente4_2 from '../imgs/componente4_2.png';

import funcionalidade1_1 from '../imgs/funcionalidade1_1.png';
import funcionalidade1_2 from '../imgs/funcionalidade1_2.png';
import funcionalidade1_3 from '../imgs/funcionalidade1_3.png';
import funcionalidade2_1 from '../imgs/funcionalidade2_1.png';
import funcionalidade2_2 from '../imgs/funcionalidade2_2.png';
import funcionalidade2_3 from '../imgs/funcionalidade2_3.png';
import funcionalidade2_4 from '../imgs/funcionalidade2_4.png';
import funcionalidade2_5 from '../imgs/funcionalidade2_5.png';
import funcionalidade2_6 from '../imgs/funcionalidade2_6.png';
import funcionalidade2_7 from '../imgs/funcionalidade2_7.png';
import funcionalidade2_8 from '../imgs/funcionalidade2_8.png';
import funcionalidade2_9 from '../imgs/funcionalidade2_9.png';
import funcionalidade2_10 from '../imgs/funcionalidade2_10.png';
import funcionalidade2_11 from '../imgs/funcionalidade2_11.png';
import funcionalidade2_12 from '../imgs/funcionalidade2_12.png';
import funcionalidade2_13 from '../imgs/funcionalidade2_13.png';
import funcionalidade2_14 from '../imgs/funcionalidade2_14.png';

import intro1_1 from '../imgs/intro1_1.png';
import termos1_1 from '../imgs/termos1_1.png';

import creditos1 from '../imgs/creditos1.png';
import creditos2 from '../imgs/creditos2.png';
import logoEita from '../imgs/logoEita.png';

export default function ModalSobre(props) {
    return (
        <Modal
            show={props.openModalSobre}
            onHide={() => {
                if (props.openModalSobre) {
                    props.onChangeOpenModalSobre(false);
                }
            }}
            size='xl'
            scrollable={true}
            fullscreen={false}
            className="modalSobreWrapper"
        >
            <Modal.Header closeButton>
                <Modal.Title>Plataforma Nimuendajú</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Tabs
                    defaultActiveKey="apresentacao"
                    id="modalSobreTabs"
                    className="mb-3"
                >
                    <Tab eventKey="apresentacao" title="Apresentação">
                        <p>A <i>Plataforma Nimuendajú</i> é a versão para web do Mapa Etno-Histórico do Brasil e Regiões Adjacentes, concebido e desenhado pelo etnólogo Curt Nimuendajú (1883-1945) na primeira metade do século XX. Objetiva permitir a interação do usuário com o Mapa em ambiente digital por meio de consultas diretas ao banco de dados que foi elaborado a partir das informações contidas nos documentos originais e nas versões impressas (mapas e livros). Foram desenvolvidos filtros e camadas inter-relacionadas que aprimoram a fruição desse valioso documento-monumento sobre a história, a territorialidade dos povos e das línguas indígenas no Brasil.</p>
                        <Figure>
                            <Figure.Image
                                src={intro1_1}
                                alt="Imagem da área total da Plataforma"
                            />
                            <Figure.Caption>
                                Imagem da área total da Plataforma
                            </Figure.Caption>
                        </Figure>
                    </Tab>


                    <Tab eventKey="sobre" title="Sobre o mapa">
                        <p>Curt Nimuendaj&uacute; elaborou tr&ecirc;s vers&otilde;es do Mapa Etno-Hist&oacute;rico do Brasil e Regi&otilde;es Adjacentes, que foram encomendadas em meados dos anos 1940 por prestigiosas institui&ccedil;&otilde;es cient&iacute;ficas: Smithsonian Institution, <a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/indl/pagina/detalhes/1567&amp;sa=D&amp;source=editors&amp;ust=1648833788811292&amp;usg=AOvVaw2KetFmCQqtf__bEk3A67o9">Museu Paraense Em&iacute;lio Goeldi</a>&nbsp;e <a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/indl/pagina/detalhes/1567&amp;sa=D&amp;source=editors&amp;ust=1648833788811572&amp;usg=AOvVaw0Y8jbbDVp1B0V2V2LVpHDa">Museu Nacional do Rio de Janeiro</a>. Esses documentos s&atilde;o testemunhos do estado da arte do conhecimento sobre os povos ind&iacute;genas do Brasil dispon&iacute;vel at&eacute; 1944. &Eacute; a s&iacute;ntese definitiva do trabalho de uma vida inteira do incans&aacute;vel pesquisador alem&atilde;o que passou a maior parte de sua vida em terras brasileiras convivendo e conhecendo profundamente povos ind&iacute;genas, entre estes, os Guarani, os Tikuna e os Kanela. Nimuendaj&uacute; catalogou cerca de 1.400 etn&ocirc;nimos ind&iacute;genas e 972 refer&ecirc;ncias bibliogr&aacute;ficas. Elaborou com os escassos recursos da &eacute;poca uma estrat&eacute;gia de classifica&ccedil;&atilde;o e de tipografia que apresenta em mesmo plano cartesiano dados sobre per&iacute;odo hist&oacute;rico, sedes abandonadas, sedes atuais, povos extintos, povos existentes e fluxos migrat&oacute;rios; utilizou tamb&eacute;m extensa paleta de cores para caracterizar as fam&iacute;lias lingu&iacute;sticas. Era consciente de que seu empreendimento n&atilde;o era definitivo, &ldquo;mas apenas uma tentativa que possa servir de base para trabalhos futuros (...), pois devia ser completado e corrigido constantemente, de acordo com os dados que v&atilde;o chegando&rdquo; (<a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/publicacao/mapaetnohistorico2ed2017.pdf&amp;sa=D&amp;source=editors&amp;ust=1648833788811880&amp;usg=AOvVaw1S3xS7Gt2XHQr3stvm0WEI">Emmerich &amp; Leite, 1981 [2017], p. 39 mapa</a>). </p>

                        <p>Esta vers&atilde;o do Mapa em formato digital visa justamente atualizar para o meio digital-informacional o acesso aos conte&uacute;dos elaborados por Nimuendaj&uacute;, com a possibilidade de associ&aacute;-los a camadas como s&iacute;tios arqueol&oacute;gicos cadastrados pelo Iphan, Terras Ind&iacute;genas, Unidades de Conserva&ccedil;&atilde;o, estados, munic&iacute;pios e biomas. Para mais informa&ccedil;&otilde;es, consultar o artigo de <a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/publicacao/mapaetnohistorico2ed2017.pdf&amp;sa=D&amp;source=editors&amp;ust=1648833788812313&amp;usg=AOvVaw1GOnjbpzJCxikl9iWY1GWh">Garcia e Lopes (2017, p. 13-17)</a>&nbsp;e a disserta&ccedil;&atilde;o de <a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/pagina/detalhes/2135&amp;sa=D&amp;source=editors&amp;ust=1648833788812667&amp;usg=AOvVaw3OgxNGZiLcR2gxogyNzxO4">Le&atilde;o (2020)</a>.</p>
                    </Tab>


                    <Tab eventKey="componentes" title="Componentes">
                        <h5>&Aacute;REA DO MAPA ETNO-HIST&Oacute;RICO GEORREFERENCIADO</h5>
                        <p>&Aacute;rea principal da Plataforma Nimuendaj&uacute; que cont&eacute;m o Mapa Etno-Hist&oacute;rico digital, georreferenciado e on-line, com links associados a linhas que informam as &aacute;reas de ocupa&ccedil;&atilde;o de cada povo ind&iacute;gena conforme consta nas refer&ecirc;ncias bibliogr&aacute;ficas consultadas pelo autor e o conhecimento emp&iacute;rico a respeito dos povos ind&iacute;genas at&eacute; o ano de 1944.</p>
                        <Figure>
                            <Figure.Image
                                src={componente1_1}
                                alt="Área total do mapa"
                            />
                            <Figure.Caption>
                                Área total do mapa
                            </Figure.Caption>
                        </Figure>


                        <h5>&Aacute;REA DE CAMADAS ADICIONAIS</h5>

                        <p>&Aacute;rea da Plataforma Nimuendaj&uacute;, no canto superior esquerdo do mapa, que cont&eacute;m op&ccedil;&otilde;es de sele&ccedil;&atilde;o de Camadas (como S&iacute;tios arqueol&oacute;gicos, Biomas, Territ&oacute;rios ind&iacute;genas, Unidades de Conserva&ccedil;&atilde;o) com informa&ccedil;&otilde;es relacionadas ao Mapa Etno-Hist&oacute;rico. Ver informa&ccedil;&otilde;es detalhadas mais abaixo.</p>

                        <ol>
                            <li>detalhe de aplica&ccedil;&atilde;o da ferramenta zoom em uma regi&atilde;o espec&iacute;fica</li>
                            <li>janela contendo as camadas S&iacute;tios arqueol&oacute;gicos, Biomas, Territ&oacute;rios ind&iacute;genas, Unidades de Conserva&ccedil;&atilde;o;</li>
                            <li>Clicando-se tanto em uma &aacute;rea como em alguma linha ser&aacute; acionada uma janela pop-up com detalhes sobre o objeto selecionado. </li>
                        </ol>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={componente2_1}
                                        alt="(1)"
                                    />
                                    <Figure.Caption>
                                        (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={componente2_2}
                                        alt="(2)"
                                    />
                                    <Figure.Caption>
                                        (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={componente2_3}
                                        alt="(3)"
                                    />
                                    <Figure.Caption>
                                        (3)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>

                        <h5>&Aacute;REAS DE BUSCA</h5>

                        <p>&Aacute;reas da Plataforma Nimuendaj&uacute;: no canto superior direito (1) e na lateral esquerda (2) do Mapa, h&aacute; motores de busca&nbsp;para conte&uacute;dos dispon&iacute;veis no Mapa Etno-Hist&oacute;rico.</p>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={componente3_1}
                                        alt="(1)"
                                    />
                                    <Figure.Caption>
                                        (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={componente3_2}
                                        alt="(2)"
                                    />
                                    <Figure.Caption>
                                        (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>


                        <h5>CAMPOS DE INFORMAÇÕES SOBRE A PLATAFORMA NIMUENDAJÚ</h5>

                        <p>Campo da Plataforma Nimuendaj&uacute;, situado no canto superior direito (1) do Mapa, que cont&eacute;m um link que abre uma janela suspensa com todas as informa&ccedil;&otilde;es t&eacute;cnicas e cr&eacute;ditos de produ&ccedil;&atilde;o desta Plataforma do Mapa Etno-Hist&oacute;rico (2). Essa janela apresenta 7 abas, com o seguinte conte&uacute;do: Apresenta&ccedil;&atilde;o, Sobre o mapa, Componentes, Funcionalidades, Cr&eacute;ditos, Refer&ecirc;ncias, Termos de Uso.</p>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={componente4_1}
                                        alt="(1)"
                                    />
                                    <Figure.Caption>
                                        (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={componente4_2}
                                        alt="(2)"
                                    />
                                    <Figure.Caption>
                                        (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>
                    </Tab>


                    <Tab eventKey="funcionalidades" title="Funcionalidades">
                        <h5>CONSULTA DIRETA NO MAPA</h5>

                        <p>&Eacute; poss&iacute;vel consultar os dados do Mapa Etno-Hist&oacute;rico diretamente sobre a imagem da &aacute;rea do mapa. Para isso, ao clicar sobre qualquer uma das linhas desenhadas sobre ele, uma janela se abre com as respectivas informa&ccedil;&otilde;es do povo ind&iacute;gena: Fam&iacute;lia lingu&iacute;stica, Situa&ccedil;&atilde;o da etnia/povo, Situa&ccedil;&atilde;o da sede, Ano (data&ccedil;&atilde;o inicial e final), Refer&ecirc;ncia de lugar, Macrorregi&atilde;o, Bioma, Estado, Unidade de conserva&ccedil;&atilde;o, Quadrante, Munic&iacute;pio e Refer&ecirc;ncias (estas refer&ecirc;ncias s&atilde;o as mesmas utilizadas por Nimuendaj&uacute; como fonte da pesquisa original. Boa parte destas refer&ecirc;ncias cont&eacute;m links ativos para o respectivo texto dispon&iacute;vel na internet.</p>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade1_1}
                                        alt="(1)"
                                    />
                                    <Figure.Caption>
                                        (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade1_2}
                                        alt="(2)"
                                    />
                                    <Figure.Caption>
                                        (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade1_3}
                                        alt="(3)"
                                    />
                                    <Figure.Caption>
                                        (3)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>

                        <h5>CONSULTA COM MOTOR DE BUSCA</h5>

                        <p>&Eacute; poss&iacute;vel tamb&eacute;m consultar os dados do Mapa Etno-Hist&oacute;rico a partir de motores de busca. Para isso, h&aacute; duas op&ccedil;&otilde;es:</p>

                        <h5>1. Busca livre</h5>

                        <p>Na caixa de busca livre (localizada no canto superior direito da tela), &eacute; poss&iacute;vel digitar uma palavra (por exemplo, etn&ocirc;mio de um povo, o nome de l&iacute;ngua, o nome de um autor ou de um t&iacute;tulo da refer&ecirc;ncia bibliogr&aacute;fica, ou uma data) e pressionar o bot&atilde;o com imagem de lupa; o resultado da busca aparece diretamente no Mapa.</p>
                        <Figure>
                            <Figure.Image
                                src={funcionalidade2_1}
                                alt="1. Busca livre"
                            />
                            <Figure.Caption>
                                1. Busca livre
                            </Figure.Caption>
                        </Figure>


                        <h5>2. Pesquisa tem&aacute;tica</h5>

                        <p>A pesquisa tem&aacute;tica, dispon&iacute;vel nos campos do painel lateral esquerdo do Mapa, permite buscar informa&ccedil;&otilde;es a partir dos seguintes filtros: Localiza&ccedil;&atilde;o, Etnia/Povo, Fam&iacute;lia lingu&iacute;stica, Situa&ccedil;&atilde;o da etnia/povo, Situa&ccedil;&atilde;o da sede, Data&ccedil;&atilde;o&nbsp;inicial, Data&ccedil;&atilde;o final, Refer&ecirc;ncia.</p>
                        <Figure>
                            <Figure.Image
                                src={funcionalidade2_2}
                                alt="2. Pesquisa tem&aacute;tica"
                            />
                            <Figure.Caption>
                                2. Pesquisa tem&aacute;tica
                            </Figure.Caption>
                        </Figure>

                        <h6>Filtro 1: Localiza&ccedil;&atilde;o</h6>

                        <p>Cont&eacute;m uma lista com informa&ccedil;&otilde;es espaciais para serem usadas como filtro: Macrorregi&atilde;o (Norte, Nordeste, Centro-Oeste, Sudeste e Sul), Bioma (Amaz&ocirc;nia, Caatinga, Cerrado, Mata Atl&acirc;ntica, Pampa e Pantanal), Estado, Unidades de Conserva&ccedil;&atilde;o, &nbsp;Quadrantes e Munic&iacute;pios. Em rela&ccedil;&atilde;o aos quadrantes, eles foram usados nos Mapas originais (1943 e 1944), registrados com letras para as colunas (de A a I) e n&uacute;meros para as linhas (de 1 a 10), &agrave; maneira de coordenadas geogr&aacute;ficas, a fim de facilitar a referencia&ccedil;&atilde;o e a localiza&ccedil;&atilde;o a partir das informa&ccedil;&otilde;es constantes no Livro do Mapa. A pesquisa pode ser feita digitando um termo na caixa de busca ou selecionando na lista suspensa.</p>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade2_3}
                                        alt="Filtro de localização (1)"
                                    />
                                    <Figure.Caption>
                                        Filtro de localização (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade2_4}
                                        alt="Filtro de localização (2)"
                                    />
                                    <Figure.Caption>
                                        Filtro de localização (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>

                        <h6>Filtro 2: Etnia/povo</h6>
                        <p>Cont&eacute;m uma lista com todos os etn&ocirc;mios inventariados por Nimuendaj&uacute; e inseridas no Mapa Etno-Hist&oacute;rico e no Livro que o acompanha. A pesquisa pode ser feita digitando um termo na caixa de busca ou selecionando na lista suspensa.</p>
                        <Figure>
                            <Figure.Image
                                src={funcionalidade2_5}
                                alt="Filtro de Etnia/Povo"
                            />
                            <Figure.Caption>
                                Filtro de Etnia/Povo
                            </Figure.Caption>
                        </Figure>

                        <h6>Filtro 3: Fam&iacute;lia lingu&iacute;stica</h6>
                        <p>Cont&eacute;m uma lista com todas as fam&iacute;lias lingu&iacute;sticas das etnias/povos inventariados por Nimuendaj&uacute; e inseridas no Mapa Etno-Hist&oacute;rico (legenda) e no Livro que o acompanha. A pesquisa pode ser feita digitando um termo na caixa de busca ou selecionando na lista suspensa.</p>
                        <Figure>
                            <Figure.Image
                                src={funcionalidade2_6}
                                alt="Filtro de Família Linguística"
                            />
                            <Figure.Caption>
                                Filtro de Família Linguística
                            </Figure.Caption>
                        </Figure>


                        <h6>Filtro 4: Situa&ccedil;&atilde;o da etnia/povo</h6>
                        <p>Cont&eacute;m uma lista com as duas possibilidades da situa&ccedil;&atilde;o da etnia/povo (Existente ou Extinta), sugeridas por Nimuendaj&uacute; no Mapa Etno-Hist&oacute;rico e no Livro que o acompanha. A pesquisa pode ser feita digitando um termo na caixa de busca ou selecionando na lista suspensa.</p>
                        <Figure>
                            <Figure.Image
                                src={funcionalidade2_7}
                                alt="Filtro de Situação da Etnia/Povo"
                            />
                            <Figure.Caption>
                                Filtro de Situação da Etnia/Povo
                            </Figure.Caption>
                        </Figure>


                        <h6>Filtro 5: Situa&ccedil;&atilde;o da sede</h6>

                        <p>Cont&eacute;m uma lista com as tr&ecirc;s possibilidades da situa&ccedil;&atilde;o da sede da etnia/povo (Abandonada, Atual ou Sem Classifica&ccedil;&atilde;o), usadas por Nimuendaj&uacute; no Mapa Etno-Hist&oacute;rico e no Livro que o acompanha. A pesquisa pode ser feita digitando um termo na caixa de busca ou selecionando na lista suspensa.</p>
                        <Figure>
                            <Figure.Image
                                src={funcionalidade2_8}
                                alt="Filtro de Situação da Sede"
                            />
                            <Figure.Caption>
                                Filtro de Situação da Sede
                            </Figure.Caption>
                        </Figure>


                        <h6>Filtros 6 e 7: Data&ccedil;&atilde;o inicial e Data&ccedil;&atilde;o final</h6>

                        <p>Cont&eacute;m uma lista organizada em s&eacute;culos ou d&eacute;cadas, para filtrar os dados por data. No filtro 6, h&aacute; a &ldquo;Data&ccedil;&atilde;o inicial&rdquo; e no filtro 7, a &ldquo;Data&ccedil;&atilde;o final&rdquo;. A pesquisa pode ser feita digitando um termo na caixa de busca ou selecionando na lista suspensa.</p>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade2_9}
                                        alt="Datação Inicial e Final (1)"
                                    />
                                    <Figure.Caption>
                                        Datação Inicial e Final (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade2_10}
                                        alt="Datação Inicial e Final (2)"
                                    />
                                    <Figure.Caption>
                                        Datação Inicial e Final (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade2_11}
                                        alt="Datação Inicial e Final (3)"
                                    />
                                    <Figure.Caption>
                                        Datação Inicial e Final (3)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>

                        <h6>Filtro 8: Refer&ecirc;ncia</h6>

                        <p>Cont&eacute;m uma lista com todas as 973 refer&ecirc;ncias documentais utilizadas por Nimuendaj&uacute; em sua pesquisa, e que foram reunidas no Livro do Mapa, seguindo uma numera&ccedil;&atilde;o sequencial. A pesquisa pode ser feita digitando um termo na caixa de busca ou selecionando na lista suspensa.</p>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade2_12}
                                        alt="Referência (1)"
                                    />
                                    <Figure.Caption>
                                        Referência (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={funcionalidade2_13}
                                        alt="Referência (2)"
                                    />
                                    <Figure.Caption>
                                        Referência (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>

                        <h6>Limpar Filtros</h6>

                        <p>Mecanismo destinado a desativa&ccedil;&atilde;o de todos os filtros j&aacute; selecionados nas caixas de busca do Mapa.</p>

                        <Figure>
                            <Figure.Image
                                src={funcionalidade2_14}
                                alt="Limpar Filtros"
                            />
                            <Figure.Caption>
                                Limpar Filtros
                            </Figure.Caption>
                        </Figure>


                        <h5>CAMADAS ADICIONAIS</h5>

                        <p>Al&eacute;m das informa&ccedil;&otilde;es do Mapa Etno-Hist&oacute;rico de Curt Nimuendaj&uacute;, esta Plataforma tamb&eacute;m disp&otilde;e das seguintes Camadas de informa&ccedil;&otilde;es: S&iacute;tios Arqueol&oacute;gicos, Biomas, Territ&oacute;rios Ind&iacute;genas, Unidades de Conserva&ccedil;&atilde;o. Ao clicar sobre uma das op&ccedil;&otilde;es, o Mapa aciona os respectivos campos, junto com as informa&ccedil;&otilde;es do Mapa. Os campos podem ser acionados simultaneamente.</p>
                        <Row>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={camadas1_1}
                                        alt="(1)"
                                    />
                                    <Figure.Caption>
                                        (1)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                            <Col>
                                <Figure>
                                    <Figure.Image
                                        src={camadas1_2}
                                        alt="(2)"
                                    />
                                    <Figure.Caption>
                                        (2)
                                    </Figure.Caption>
                                </Figure>
                            </Col>
                        </Row>


                        <h6>Camada 1: S&iacute;tios Arqueol&oacute;gicos</h6>

                        <p>Camada que adiciona ao Mapa da Plataforma campos de S&iacute;tios Arqueol&oacute;gicos do Brasil, a partir da Base do Iphan contendo os seguintes dados: Nome do S&iacute;tio, Classifica&ccedil;&atilde;o, Natureza, Tipo de bem, Data do Cadastro, S&iacute;ntese.</p>
                        <Figure>
                            <Figure.Image
                                src={camadas2_1}
                                alt="Camada de Sítios Arqueológicos"
                            />
                            <Figure.Caption>
                                Camada de Sítios Arqueológicos
                            </Figure.Caption>
                        </Figure>

                        <h6>Camada 2: Biomas</h6>

                        <p>Camada que adiciona ao Mapa da Plataforma campos de Biomas do Brasil, a partir da Base do IBGE: Amaz&ocirc;nia, Caatinga, Cerrado, Mata Atl&acirc;ntica, Pampa e Pantanal</p>
                        <Figure>
                            <Figure.Image
                                src={camadas3_1}
                                alt="Camada de Biomas"
                            />
                            <Figure.Caption>
                                Camada de Biomas
                            </Figure.Caption>
                        </Figure>

                        <h6>Camada 3: Territ&oacute;rios Ind&iacute;genas</h6>

                        <p>Camada que adiciona ao Mapa da Plataforma campos de Territ&oacute;rios Ind&iacute;genas do Brasil, a partir da Base da Funai: Etnia, Munic&iacute;pio, UF, Modalidade, Fase do processo fundi&aacute;rio, &Aacute;rea (Km&sup2;)</p>
                        <Figure>
                            <Figure.Image
                                src={camadas4_1}
                                alt="Camada de Territórios Indígenas"
                            />
                            <Figure.Caption>
                                Camada de Territórios Indígenas
                            </Figure.Caption>
                        </Figure>

                        <h6>Camada 4: Unidades de Conserva&ccedil;&atilde;o</h6>

                        <p>Camada que adiciona ao Mapa da Plataforma campos de Unidades de Conserva&ccedil;&atilde;o do Brasil, a partir da Base do Iphan: Nome, Categoria, Inst&acirc;ncia, Ano de cria&ccedil;&atilde;o, UF, Bioma, Bacia Hidrogr&aacute;fica, &Aacute;rea (Km&sup2;), Gest&atilde;o</p>
                        <Figure>
                            <Figure.Image
                                src={camadas5_1}
                                alt="Camada de Unidades de Conservação"
                            />
                            <Figure.Caption>
                                Camada de Unidades de Conservação
                            </Figure.Caption>
                        </Figure>
                    </Tab>



                    <Tab eventKey="creditos" title="Créditos">
                        <img src={creditos1} alt="Instituto do Patrimônio Histórico e Artístico Nacional (Iphan)" />
                        <h5><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/&amp;sa=D&amp;source=editors&amp;ust=1648833788829704&amp;usg=AOvVaw1Y_U0QiwjN_bGRMp5fOURB">INSTITUTO DO PATRIM&Ocirc;NIO HIST&Oacute;RICO E ART&Iacute;STICO NACIONAL (IPHAN)</a></h5>
                        <p>Presidente: Larissa Peixoto</p>
                        <p>Diretor de Patrim&ocirc;nio Imaterial: Roger Alves</p>
                        <p>Coordenador-Geral de Identifica&ccedil;&atilde;o e Registro: Cassiano Luis Boldori</p>
                        <p>Chefe da Divis&atilde;o T&eacute;cnica de Diversidade Lingu&iacute;stica: Marcus Vin&iacute;cius C. Garcia</p>
                        <p>Equipe DTDL/DPI: Tha&iacute;s Werneck, Daniel Ara&uacute;jo</p>
                        <p>Programa&ccedil;&atilde;o de banco de dados e georreferenciamento: Pedro Youssef</p>
                        <p>Mestranda do Programa Especializa&ccedil;&atilde;o em Patrim&ocirc;nio/Iphan: Ana Carolina Le&atilde;o</p>
                        <p>Estagi&aacute;rias: Braulina Aurora Baniwa e Nayra Kaxuyana</p>

                        <img className="creditos2" src={creditos2} alt="Universidade Federal do Pará" />
                        <h5><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://portal.ufpa.br/&amp;sa=D&amp;source=editors&amp;ust=1648833788830739&amp;usg=AOvVaw28Pm5RKjW5GivMr1WhPKV6">UNIVERSIDADE FEDERAL DO PAR&Aacute; (UFPA)</a></h5>

                        <p>Reitor: Emmanuel Zagury Tourinho</p>
                        <p>Vice-Reitor: Gilmar Pereira da Silva</p>
                        <p>Pr&oacute;-Reitora de Ensino de Gradua&ccedil;&atilde;o: Mar&iacute;lia de Nazar&eacute; de Oliveira Ferreira</p>
                        <p>Pr&oacute;-Reitora de Pesquisa e P&oacute;s-Gradua&ccedil;&atilde;o: Maria Iracilda da Cunha Sampaio</p>
                        <p>Pr&oacute;-Reitor de Extens&atilde;o: Nelson Jos&eacute; de Souza J&uacute;nior</p>

                        <h5><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://www.campuscameta.ufpa.br/&amp;sa=D&amp;source=editors&amp;ust=1648833788831399&amp;usg=AOvVaw1_uPuP3o3dLrHK60ZUOyk7">Campus Universit&aacute;rio do Tocantins/Camet&aacute;</a></h5>
                        <p>Coordenadora: Maria Lucilena Gonzaga Costa Tavares</p>
                        <p>Vice-Coordenador: Eraldo Souza do Carmo</p>
                        <p>Coordenador do Centro de Pesquisa: Jo&atilde;o Batista do Carmo Silva</p>
                        <p>Coordenador de Extens&atilde;o: M&aacute;rio J&uacute;nior de Carvalho Arnaud</p>
                        <h5>Projeto Plataforma Interativa Mapa Etno-Hist&oacute;rico Curt Nimuendaju</h5>
                        <p>Coordenador do Projeto (UFPA): <a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://lattes.cnpq.br/5788146269243089&amp;sa=D&amp;source=editors&amp;ust=1648833788832155&amp;usg=AOvVaw1HK1wmnfXVgyc9OF3h52hi">Jorge Domingues Lopes</a></p>
                        <p>Estagi&aacute;rios da primeira fase: Fernando Luiz Pompeu Varela e Leonardo do Socorro do Carmo Sanches</p>
                        <p>Estagi&aacute;rios da segunda fase: Amanda Ramos da Silva, Benedita Alho da Silva e Brendo Garcia Batista</p>


                        <h5>AGRADECIMENTOS</h5>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://www.ibge.gov.br/&amp;sa=D&amp;source=editors&amp;ust=1648833788832913&amp;usg=AOvVaw0wWUOSxQzQ6zey6BIO19NK">IBGE - Instituto Brasileiro de Geografia e Estat&iacute;stica</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://www.gov.br/funai/pt-br&amp;sa=D&amp;source=editors&amp;ust=1648833788833280&amp;usg=AOvVaw0WgPDmucMebc8nUZkrm7_a">FUNAI - Funda&ccedil;&atilde;o Nacional do &Iacute;ndio</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://www.gov.br/museugoeldi/pt-br&amp;sa=D&amp;source=editors&amp;ust=1648833788833571&amp;usg=AOvVaw0kjJ2foIbeGhiA4VGXT80b">MPEG - Museu Paraense Em&iacute;lio Goeldi</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://www.museunacional.ufrj.br/&amp;sa=D&amp;source=editors&amp;ust=1648833788833924&amp;usg=AOvVaw3RyodlUBI_bgoVBxcCLUsN">MNRJ - Museu Nacional da Universidade Federal do Rio de Janeiro</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://www.facebook.com/LALLIUnB/&amp;sa=D&amp;source=editors&amp;ust=1648833788834221&amp;usg=AOvVaw3jxaFKPAYZ4vRwygU_dLCy">LALLI - Laborat&oacute;rio de L&iacute;nguas e Literaturas Ind&iacute;genas/UnB</a>&nbsp;</p>

                        <h5 className="mt-3">Desenvolvido pela Cooperativa EITA</h5>
                        <img className="logoEita" src={logoEita} alt="Cooperativa EITA" />
                    </Tab>



                    <Tab eventKey="referencias" title="Links Úteis">
                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/publicacao/mapaetnohistorico2ed2017.pdf&amp;sa=D&amp;source=editors&amp;ust=1648833788835548&amp;usg=AOvVaw26_JhwQCUl9I9Bf8c5Ieeh">Livro Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Curt Nimuendaju - 2.&ordf; edi&ccedil;&atilde;o 2017</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://online.flippingbook.com/view/894788/&amp;sa=D&amp;source=editors&amp;ust=1648833788836050&amp;usg=AOvVaw0fk2nI8fOj7XbpsnZEGAp8">Livro Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Curt Nimuendaju - 2.&ordf; edi&ccedil;&atilde;o 2017 em formato Flipbook</a>&nbsp;</p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/ckfinder/arquivos/Mapa_Nimuendaju_2017.pdf&amp;sa=D&amp;source=editors&amp;ust=1648833788836655&amp;usg=AOvVaw04spOYbGaQ8SQMaqhrZCKs">Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Curt Nimuendaju - 2017</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/ckfinder/arquivos/mapa_nimuendaju1943_m.pdf&amp;sa=D&amp;source=editors&amp;ust=1648833788837314&amp;usg=AOvVaw2tHC2vTg9VgU7dtrol5NJK">Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Museu Paraense Em&iacute;lio Goeldi - Curt Nimuendaju - 1943</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/ckfinder/arquivos/mapa_etno-historico_museu_nacional_restauracao_digital.zip&amp;sa=D&amp;source=editors&amp;ust=1648833788837900&amp;usg=AOvVaw2j5Dsv6JgwpdTtz7zoPNpZ">Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Museu Nacional do Rio de Janeiro - Curt Nimuendaju - 1944</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://www.youtube.com/watch?v%3Dmqzy722O_V4&amp;sa=D&amp;source=editors&amp;ust=1648833788838378&amp;usg=AOvVaw1dENYDgG96K6STi8ZC9TSE">Mapa Etno-hist&oacute;rico do Brasil e Regi&otilde;es Adjacentes. Breve document&aacute;rio sobre &nbsp;o Projeto.</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/ckfinder/arquivos/Livro_e_MapaNimuendaju1981.zip&amp;sa=D&amp;source=editors&amp;ust=1648833788838829&amp;usg=AOvVaw0DX7EGPM2pqfDiJYpQwRPo">Livro e Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Curt Nimuendaju - 1981</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/ckfinder/arquivos/Livro_e_Mapa_Nimuendaju1987.zip&amp;sa=D&amp;source=editors&amp;ust=1648833788839268&amp;usg=AOvVaw0ui4ixC-uLOQTH_t9KV0Jp">Livro e Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Curt Nimuendaju - 1987</a>&nbsp;</p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/uploads/ckfinder/arquivos/mapa_etnohistorico_nimuendaju_2002.pdf&amp;sa=D&amp;source=editors&amp;ust=1648833788839656&amp;usg=AOvVaw3xA525YSbdO0inLnux5krN">Mapa Etno-Hist&oacute;rico do Brasil e regi&otilde;es adjacentes - Curt Nimuendaju - 2002</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/geoserver/web/wicket/bookmarkable/org.geoserver.web.demo.MapPreviewPage?1&amp;sa=D&amp;source=editors&amp;ust=1648833788840120&amp;usg=AOvVaw35jBTsTcf3FuzW7rsma6gv">Geoserver do Iphan: dados do Mapa de Nimuendaj&uacute;</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=https://www.gov.br/funai/pt-br/atuacao/terras-indigenas/geoprocessamento-e-mapas/geprocessamento&amp;sa=D&amp;source=editors&amp;ust=1648833788840579&amp;usg=AOvVaw1tRsLqkjHBtSCPz5A9ZFJM">Site da FUNAI: Terras ind&iacute;genas georreferenciadas</a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://www.etnolinguistica.org/&amp;sa=D&amp;source=editors&amp;ust=1648833788840975&amp;usg=AOvVaw1cFhwtL5gSt2UbZmEzVquz">Site da Biblioteca Digital Curt Nimuendaj&uacute; </a></p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/pagina/detalhes/1699&amp;sa=D&amp;source=editors&amp;ust=1648833788841384&amp;usg=AOvVaw0IcLk94A6KxLrTfEDQWZLH">S&iacute;tios Arqueol&oacute;gicos georreferenciados (Iphan)</a></p>

                        <h5>Teses/Disserta&ccedil;&otilde;es:</h5>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/pagina/detalhes/2135&amp;sa=D&amp;source=editors&amp;ust=1648833788842577&amp;usg=AOvVaw2zEu8VaRVJbXOcU3oWLr4T">LE&Atilde;O, Ana Carolina Rezende. Patrim&ocirc;nio Cultural e Mapa etno-hist&oacute;rico do Brasil e regi&otilde;es adjacentes de Curt Nimuendaj&uacute;: Pol&iacute;tica P&uacute;blica e Diversidade Lingu&iacute;stica. 163 fls. Disserta&ccedil;&atilde;o (Mestrado em Preserva&ccedil;&atilde;o do Patrim&ocirc;nio Cultural) - IPHAN, Rio de Janeiro, 2020</a>.</p>

                        <p><a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://etnolinguistica.wdfiles.com/local--files/tese:welper-2002/welper_2002_curt.pdf&amp;sa=D&amp;source=editors&amp;ust=1648833788843303&amp;usg=AOvVaw0yuyiKi5qBl5SSaWu0IgKM">WELPER, Elena Monteiro. Curt Unckel Nimuendaj&uacute;: Um cap&iacute;tulo alem&atilde;o na tradi&ccedil;&atilde;o etnogr&aacute;fica brasileira. Rio de Janeiro: UFRJ/ PPGAS-.MN. 2002.</a></p>
                    </Tab>



                    <Tab eventKey="contatos" title="Contatos e novidades">
                        <h5>CONTATOS</h5>
                        <p>
                            Sugest&otilde;es, d&uacute;vidas e compartilhamento de experi&ecirc;ncias de uso da Plataforma, escreva para:
                        </p>
                        <ul>
                            <li>
                                <a target="_blank" rel="noreferrer" href="mailto:diversidade.linguistica@iphan.gov.br">
                                    diversidade.linguistica@iphan.gov.br
                                </a>
                            </li>
                            <li>
                                <a target="_blank" rel="noreferrer" href="mailto:jdlopes@ufpa.br">
                                    jdlopes@ufpa.br
                                </a>
                            </li>
                        </ul>
                        <h5>
                            REDE SOCIAL
                        </h5>
                        <p>
                            Acompanhe a rede social da Plataforma para novidades e artigos sobre o assunto!
                            <br />
                            <a target="_blank" rel="noreferrer" href="https://www.facebook.com/plataformanimuendaju">
                                https://www.facebook.com/plataformanimuendaju
                            </a>
                        </p>
                    </Tab>



                    <Tab eventKey="termos" title="Termos de Uso">
                        <p>1. OBJETIVO</p>

                        <p>Os presentes termos e condi&ccedil;&otilde;es de uso da Plataforma Interativa do Mapa Etno-Hist&oacute;rico do Brasil e Regi&otilde;es Adjacentes, de Curt Nimuendaj&uacute;, dispon&iacute;vel no endere&ccedil;o eletr&ocirc;nico <a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://mapa-nimuendaju.eita.coop.br/&amp;sa=D&amp;source=editors&amp;ust=1648833788844279&amp;usg=AOvVaw1olEKrXZyD9B6Fve5P6z_D">http://mapa-nimuendaju.eita.coop.br/</a>,&nbsp;denominado a seguir &nbsp;&ldquo;Plataforma&rdquo;, definem as condi&ccedil;&otilde;es de uso da Plataforma pelos usu&aacute;rios da internet.</p>

                        <p>Ao navegar na Plataforma, o usu&aacute;rio da Internet reconhece ter lido os avisos legais da Plataforma e aceita as presentes condi&ccedil;&otilde;es gerais de uso, aplic&aacute;veis a qualquer acesso on-line na Plataforma, com ou sem login de usu&aacute;rio.</p>

                        <p>O Instituto do Patrim&ocirc;nio Hist&oacute;rico e Art&iacute;stico Nacional e a Universidade Federal do Par&aacute; reservam-se o direito de modificar a qualquer momento estas condi&ccedil;&otilde;es gerais de uso para adapt&aacute;-las &agrave;s funcionalidades legislativas, regulamentares ou novas, da Plataforma.</p>

                        <p>Os conte&uacute;dos acess&iacute;veis nesta Plataforma s&atilde;o, em sua maioria, reprodu&ccedil;&otilde;es digitais de obras que ca&iacute;ram no dom&iacute;nio p&uacute;blico de diversas cole&ccedil;&otilde;es, segundo a lei brasileira n.&ordm; 9.610/1998. Portanto, a reutiliza&ccedil;&atilde;o n&atilde;o comercial destes conte&uacute;dos &eacute; gratuita, em conformidade com a legisla&ccedil;&atilde;o em vigor e, em particular, a manuten&ccedil;&atilde;o da men&ccedil;&atilde;o de origem dos conte&uacute;dos, conforme especificado em: <a target="_blank" rel="noreferrer" href="https://www.google.com/url?q=http://portal.iphan.gov.br/indl/pagina/detalhes/1563&amp;sa=D&amp;source=editors&amp;ust=1648833788844939&amp;usg=AOvVaw3dvMkCufncl_kMNYJSQjHq">http://portal.iphan.gov.br/indl/pagina/detalhes/1563</a></p>

                        <p>2. CONDI&Ccedil;&Otilde;ES E TERMOS DE USO</p>

                        <p>O acesso &agrave; Plataforma e todo o seu conte&uacute;do &eacute; totalmente gratuito.</p>

                        <p>Podem ser feitas consultas de todos os conte&uacute;dos dispon&iacute;veis nesta Plataforma, bem como seu compartilhamento, contanto que seja devidamente citada a fonte.</p>

                        <p>As informa&ccedil;&otilde;es dispon&iacute;veis na Plataforma podem ser usadas livremente para, por exemplo, atividades educacionais, de pesquisa, de extens&atilde;o acad&ecirc;mica, de valoriza&ccedil;&atilde;o dos povos/etnias ind&iacute;genas e suas respectivas l&iacute;nguas e culturas.</p>

                        <p>Os links de materiais dispon&iacute;veis em sites externos &agrave; Plataforma n&atilde;o s&atilde;o de nossa responsabilidade, logo eles podem eventualmente mudar de endere&ccedil;o e/ou n&atilde;o estarem nos links fornecidos. Neste caso, pode haver links quebrados. O uso desses documentos digitalizados por institui&ccedil;&otilde;es externas ao Iphan e &agrave; UFPA est&atilde;o sujeitos &agrave;s respectivas pol&iacute;ticas de uso e privacidade de cada institui&ccedil;&atilde;o mantenedora do material.</p>

                        <p>O usu&aacute;rio da internet tamb&eacute;m pode:</p>

                        <p>- fazer c&oacute;pia de fragmentos ou da &iacute;ntegra do Mapa da Plataforma, atribuindo os devidos cr&eacute;ditos,</p>

                        <p>- baixar documentos ou extratos de documentos dispon&iacute;veis na Plataforma,</p>

                        <p>- compartilhamento de dados da Plataforma em redes sociais ou em sites e blogs,</p>

                        <p>- reportar problemas t&eacute;cnicos ou bibliogr&aacute;ficos,</p>

                        <p>- entrar em contato com os administradores da Plataforma.</p>

                        <p>3. LEI APLIC&Aacute;VEL</p>

                        <p>A Plataforma e as presentes Condi&ccedil;&otilde;es Gerais de Uso est&atilde;o sujeitos &agrave; legisla&ccedil;&atilde;o brasileira.</p>

                        <img src={termos1_1} alt="Termos" />

                        <p>&Uacute;ltima atualiza&ccedil;&atilde;o: 22.02.2022</p>
                    </Tab>
                </Tabs>
            </Modal.Body>
        </Modal>
    );
}
