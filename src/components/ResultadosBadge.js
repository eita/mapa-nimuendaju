// import { useState, useEffect } from 'react';
import {
    Container,
    Badge
} from 'react-bootstrap';

export default function ResultadosBadge({qtde}) {
    return (
        <Container className="resultados">
            <Badge pill bg="resultados">
                {qtde} {(qtde === 1) ? "resultado" : "resultados"}
            </Badge>
        </Container>
    );
};
