export const isMarkerInsidePolygon = (latLng, multiPolyPoints) => {
    var x = latLng[0], y = latLng[1];

    for (let polyPointsRaw of multiPolyPoints) {
        const polyPoints = polyPointsRaw[0];
        if (!Array.isArray(polyPoints)) {
            continue;
        }
        var inside = false;
        for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
            var xi = polyPoints[i].lat, yi = polyPoints[i].lng;
            var xj = polyPoints[j].lat, yj = polyPoints[j].lng;

            var intersect = ((yi > y) !== (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }
        if (inside) {
            return true;
        }
    }

    return inside;
};

export const getHeatMapColor = (value) => {
    if (isNaN(value)) {
        return 'rgba(0,0,255,0)';
    }
    const NUM_COLORS = 4;
    const color = [ [194, 157, 189, 0.5], [174, 125, 168, 1], [153, 94, 147, 1], [132, 63, 126, 1] ];
    // A static array of 4 colors:  (blue,   green,  yellow,  red) using {r,g,b} for each.

    let i;        // |-- Our desired color will be between these two indexes in "color".
    let j;        // |
    let fractBetween = 0.0;  // Fraction between "idx1" and "idx2" where our value is.

    if (value <= 0) {
        i = j = 0;
    } else if (value >= 1) {
        i = j = NUM_COLORS-1;
    } else {
        value = value * (NUM_COLORS-1);        // Will multiply value by 3.
        i = Math.floor(value);                  // Our desired color will be after this index.
        j = i + 1;                        // ... and before this index (inclusive).
        fractBetween = value - parseFloat(i);    // Distance between the two indexes (0-1).
    }

    const red = ((color[j][0] - color[i][0]) * fractBetween) + color[i][0];
    const green = ((color[j][1] - color[i][1]) * fractBetween) + color[i][1];
    const blue  = ((color[j][2] - color[i][2]) * fractBetween) + color[i][2];
    const opacity = ((color[j][3] - color[i][3]) * fractBetween) + color[i][3];

    return `rgba(${red}, ${green}, ${blue}, ${opacity})`;
};

export const getHeatMapOpacity = (value) => {
    if (isNaN(value)) {
        return 0;
    }
    const NUM_OPACITIES = 4;
    const opacity = [ 0, 0.1, 0.5, 0.8, 1 ];

    let i;        // |-- Our desired color will be between these two indexes in "color".
    let j;        // |
    let fractBetween = 0.0;  // Fraction between "idx1" and "idx2" where our value is.

    if (value <= 0) {
        i = j = 0;
    } else if (value >= 1) {
        i = j = NUM_OPACITIES-1;
    } else {
        value = value * (NUM_OPACITIES-1);
        i = Math.floor(value);
        j = i + 1;
        fractBetween = value - parseFloat(i);
    }

    return ((opacity[j] - opacity[i]) * fractBetween) + opacity[i];
};
